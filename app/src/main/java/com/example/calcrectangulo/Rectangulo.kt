package com.example.calcrectangulo

class Rectangulo(private val base: Double, private val altura: Double) {
    private val area: Double = base * altura
    private val perimetro: Double = (base * 2) + (altura * 2)

    fun calcularArea(): Double {
        return area
    }

    fun calcularPerimetro(): Double {
        return perimetro
    }
}
